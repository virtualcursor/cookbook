<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class IngredientFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $ingredients = [];

        $ingredient = new Ingredient();
        $ingredient->setIngredient('Test ingrident');
        $manager->persist($ingredient);
        $this->addReference('ingredient-1', $ingredient);

        $ingredient = new Ingredient();
        $ingredient->setIngredient('Test ingrident');
        $manager->persist($ingredient);
        $this->addReference('ingredient-2', $ingredient);

        $manager->flush();
    }
}
