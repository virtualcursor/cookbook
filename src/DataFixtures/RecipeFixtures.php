<?php

namespace App\DataFixtures;

use App\Entity\Recipe;
use App\DataFixtures\IngredientFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RecipeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $recipe = new Recipe();

        $recipe->setTitle('Test recipe');
        $recipe->addIngredient($this->getReference('ingredient-1'));
        $recipe->addIngredient($this->getReference('ingredient-2'));

        $manager->persist($recipe);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            IngredientFixtures::class,
        );
    }
}
